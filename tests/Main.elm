port module Main exposing (..)

import Json.Encode exposing (Value)
import Test exposing (..)
import Test.Runner.Node exposing (TestProgram, run)
import Test.UTF8 as UTF8


tests : Test
tests =
    describe "Tests for UTF-8 encode/decode library"
        [ UTF8.tests
        ]


main : TestProgram
main =
    run emit tests


port emit : ( String, Value ) -> Cmd msg
