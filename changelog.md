# 1.0.1

### Upgraded to 0.18

# 1.0.0

### Initial release

  * Added the following functions to `UTF8`:

	toMultiByte : String -> String
	toSingleByte : String -> String
